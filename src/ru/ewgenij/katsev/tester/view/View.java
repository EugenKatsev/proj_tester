package ru.ewgenij.katsev.tester.view;

//Все кто имплементируют/наследуют интерфейс view, имеют метод start
public interface View {
    //старт приложения, старт вывода меню
    void start();

}



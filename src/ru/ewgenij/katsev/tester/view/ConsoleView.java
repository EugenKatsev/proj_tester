package ru.ewgenij.katsev.tester.view;

import ru.ewgenij.katsev.tester.controller.IController;
import ru.ewgenij.katsev.tester.controller.TestController;

import java.util.List;
import java.util.Scanner;



public class ConsoleView implements View {
    private Scanner strAutomatikReader;
    private IController businessLogika;


    //Создаем централизованный  один конструктор, а ниже будем им пользоваться
    public ConsoleView(){
        strAutomatikReader = new Scanner(System.in); //Инитиально читай в консоле дефинируем 1 раз
        businessLogika = new TestController();
    }


    @Override
    public void start() {
        //Это главное меню программы в котором полбзователь делает выбор
        System.out.println("Приветствую вас в программе Тестер!");
        while (true){
            System.out.println("Сделайте ваш выбор:");
            System.out.println("Создать тест введите 1");
            System.out.println("Пройти тест введите 2");
            System.out.println("Удалить тест введите 3");
            System.out.println("Выход введите 4");

            int userChoise= getIntInput(1, 4);

            //в зависимости от выбора срабатывает нужный кейс
            switch (userChoise){
                case 1:
                    createTest();
                    break;
                case 2:
                    runTest();
                    break;
                case 3:
                    deleteTest();
                    break;
                case 4:
                    System.exit(1);
                    break;
            }
        }

    }

    //если пользователь выбрал "2 пройти тест"
    private void runTest() {
        int result = getChosenTestIndex();
        if(result > 0) {
            businessLogika.runTest(result - 1);
            while (businessLogika.hasNextQuestion()/*Если Правда*/){
                System.out.println(businessLogika.getQuestion());
                String[] answers = businessLogika.getAnswers();
                for(int i=0; i<answers.length;i++){
                    System.out.println(answers[i]);
                }
                int userAnswer = getIntInput(1, answers.length);
                businessLogika.sendUserAnswer(userAnswer);
            }
            System.out.println(businessLogika.getResult());

        }
    }
    //если пользователь выбрал "3 удалить тест"
    private void deleteTest() {
        int result = getChosenTestIndex();


        if(result > 0) {
            businessLogika.deleteTest(result - 1);
            System.out.println(String.format("Тест No %d Успешно удален ", result));
        }




    }
    //выводим список имеющихся тестов
    private int getChosenTestIndex() {
        List<String> testNames = businessLogika.getTestNames();
        if (testNames == null) {
            System.out.println("В системе еще нет тестов!");
            return 0;
        }else {
            System.out.println("Выберите тест из списка, или 0 для возврата в меню");
            for(int i = 0; i<testNames.size();i++) {
                System.out.println((i +1) + ". " + testNames.get(i));

            }
            //тут перехватываем какой номер теста ввел пользователь и возвращаем его индекс
            //тут мы должны допустить 0 для возврата в меню
            return getIntInput(0, testNames.size());


        }
    }





    //если пользователь выбрал "1 создать тест"
    private void createTest() {


        System.out.println("Пожлуйста введите author:");
        String author=strAutomatikReader.nextLine();
        System.out.println("Пожлуйста введите testName:");
        String testName = strAutomatikReader.nextLine();

        System.out.println("Cколько вопросов в тесте:");
        int questionsCount;

        while(true) {
            try {
                questionsCount = Integer.valueOf(strAutomatikReader.nextLine());
                break;
            } catch(Exception e){
                System.out.println("Введите правильное значение!");
            }

        }


        String[] questions = new String[questionsCount];
        String[][] answers = new String [questionsCount][];
        int[] correctAnswers = new int[questionsCount];

        for (int i = 0; i<questions.length; i++){
            //1из3 пунктов для создания теста
            System.out.println("Введите вопрос номер " + (i+1));
            questions[i]=strAutomatikReader.nextLine();
            //2из3 пунктов для создания теста
            String[] currentAnswers = new String[2];
            for (int g = 0; g<currentAnswers.length; g++){
                System.out.println("Введите ответ номер " + (g+1) + " для вопроса номер " + (i+1));
                currentAnswers[g]=strAutomatikReader.nextLine();
            }
            answers[i] = currentAnswers;
            //3из3 пунктов для создания теста

            while(true){
                try {
                    System.out.println("Введите правильный номер ответа: ");
                    correctAnswers[i] = Integer.valueOf(strAutomatikReader.nextLine());
                    if(correctAnswers[i]<1 || correctAnswers [i] > answers[i].length){
                        System.out.println("Введите корректное значение в границах ответов");
                        continue; //Возвращает в начало цикла
                    }
                    break; //Прерывает цикл
                } catch(Exception e){
                    System.out.println("Введено недопустимое значение!");
                }
            }


        }


        //передаем параметры только что созданнного теста для создаения обеькта теста
        businessLogika.createTest(testName, author, questions, answers, correctAnswers);

    }

    //сам перехват того, что вводит пользователь (например при выборе теста)
    private int getIntInput (int down, int up){

        int userChoise;
        while(true){
            try {
                userChoise  = Integer.valueOf(strAutomatikReader.nextLine());
                if(userChoise < down || userChoise  > up){
                    System.out.println(String.format( "Введено недопустимое значение, повторите ввод (%d до %d)", down, up));
                    continue; //Возвращает в начало цикла
                }
                break; //Прерывает цикл
            } catch(Exception e){
                System.out.println(String.format( "Введено недопустимое значение, повторите ввод (%d до %d)", down, up));
            }
        }
        //и возвращает выбранные индекс (не обязательно тест, может и быть пункт в меню)
        return userChoise;
    }
}

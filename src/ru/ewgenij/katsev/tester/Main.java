package ru.ewgenij.katsev.tester;


import ru.ewgenij.katsev.tester.view.ConsoleView;
import ru.ewgenij.katsev.tester.view.View;

public class Main {
    public static void main(String[] args) {
        View view = null;
        if (args[0].equalsIgnoreCase("Console")){
            view = new ConsoleView();
        }

        if (view != null) {
            view.start();
        } else {
            System.exit(0);
        }


    }

}

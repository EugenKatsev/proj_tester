package ru.ewgenij.katsev.tester.controller;

import com.google.gson.Gson;
import ru.ewgenij.katsev.tester.model.Test;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class JDBCDataController implements IDataController {
    private static final String DB_DRIVER= "oracle.jdbc.driver.OracleDriver";
    private static final String DB_URL= "jdbc:oracle:thin:@localhost:1521:xe";
    private static final String DB_USER= "SYSTEM";
    private static final String DB_PASSWORD= "root1";
    private Connection connection;
    private Gson gson;


    JDBCDataController(){
        gson = new Gson();
        try {
            Locale.setDefault(Locale.GERMANY);
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean createTest(Test test) {
        try {
            String s = gson.toJson(test);
            System.out.println(s);
            Statement statement = connection.createStatement();
            statement.executeQuery(String.format("Insert into tbl_test1 (testname, testbody) values ('%s', '%s')", test.getTestName(), gson.toJson(test)));
            statement.close();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public boolean deleteTest(Test test) {
        try {
            Statement statement = connection.createStatement();
            statement.executeQuery(String.format("Delete from tbl_test1 t where t.testname = '%s'", test.getTestName()));
            statement.close();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }



    }

    @Override
    public List<Test> getTestList() {
        List<Test> testList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select * from tbl_test1");
            while (rs.next()) {
                testList.add(gson.fromJson(rs.getString(2), Test.class));
            }
            rs.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return testList;
    }
}

package ru.ewgenij.katsev.tester.controller;

import ru.ewgenij.katsev.tester.model.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileSystemDataController implements IDataController{

    private static final String EXTENTION=".test";
    private static final String TEST_FOLDER="tests";

    //Создаем папку в которой будут храниться тесты
    FileSystemDataController(){
        File dir = new File(TEST_FOLDER);
        if (!dir.isDirectory()){
            dir.delete();
        }
        if (!dir.exists()){
            dir.mkdir();
        }
    }

    @Override
    public boolean createTest(Test test) {

        File file = new File(TEST_FOLDER + File.separator + test.getTestName() + EXTENTION);

        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(test);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean deleteTest(Test test) {
        File file = new File(TEST_FOLDER + File.separator + test.getTestName() + EXTENTION);
        return file.delete();

    }

    @Override
    public List<Test> getTestList() {
        File dir = new File (TEST_FOLDER);
        File [] files = dir.listFiles();
        List <Test> result = new ArrayList<>();
        for (int i=0; i<files.length;i++){
            try (FileInputStream fis = new FileInputStream(files[i]);
                ObjectInputStream ois = new ObjectInputStream(fis)){

                Test test = (Test) ois.readObject();

                result.add(test);

            } catch (Exception e) {
                e.printStackTrace();

            }
        }


        return result;
    }
}

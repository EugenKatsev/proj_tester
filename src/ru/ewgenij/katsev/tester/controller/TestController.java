package ru.ewgenij.katsev.tester.controller;

import ru.ewgenij.katsev.tester.model.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestController implements IController{
    private IDataController dataController;
    //в нем будут хранится все обекты типа тест.
    private List<Test> testList;
    private Test currentTest;
    private int currentQuestionNumber=0;
    private int correctAnswers=0;


    //Создаем централизованный  один конструктор, а ниже будем им пользоваться

    public TestController(){
        //dataController = new FileSystemDataController();
        dataController = new JDBCDataController();


    }





    @Override
    public void createTest(String inputTestName, String inputAuthor, String[] inputQuestions, String[][] inputAnswers, int[] inputCorrectAnswers) {
        //новый только что созданые обьект типа Тест добавляется в список testList, и хранится в оперативке
        dataController.createTest(new Test(inputTestName, inputAuthor, inputQuestions, inputAnswers, inputCorrectAnswers));




    }

    @Override
    public void deleteTest(int index) {
        if (index >= 0 && index < testList.size()) {
            dataController.deleteTest(testList.get(index));
        }


    }

    @Override
    public void runTest(int index) {
        currentTest = testList.get(index);
        currentQuestionNumber = 0;
        correctAnswers = 0;
    }

    @Override
    public List<String> getTestNames() {
        testList = dataController.getTestList();
        if (!testList.isEmpty()){
            return testList.stream().map(Test::getTestName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean hasNextQuestion() {
        return currentQuestionNumber < currentTest.getQuestions().length;
    }

    @Override
    public String getQuestion() {
        return currentTest.getQuestions() [currentQuestionNumber];
    }

    @Override
    public String[] getAnswers() {
        return currentTest.getAnswers()[currentQuestionNumber];
    }

    @Override
    public void sendUserAnswer(int userAnswer) {
        if(userAnswer == currentTest.getCorrectAnswers() [currentQuestionNumber]){
            correctAnswers++;
        }
        currentQuestionNumber++;

    }

    @Override
    public int getResult() {
        return correctAnswers * 100 / currentTest.getQuestions().length;
    }
}


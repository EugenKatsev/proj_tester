package ru.ewgenij.katsev.tester.controller;

import ru.ewgenij.katsev.tester.model.Test;

import java.util.List;

public interface IDataController {

    boolean createTest(Test test);
    boolean deleteTest(Test test);
    List<Test> getTestList();

}

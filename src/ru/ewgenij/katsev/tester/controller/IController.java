package ru.ewgenij.katsev.tester.controller;

import java.util.List;

public interface IController {
    // если void то нет возвращаемого значения!!!

    /**
     * этот метод используется для создания тестов
     * @param inputTestName - поле название теста
     * @param inputAuthor - поле имя автора теста
     * @param inputQuestions - список вопрос в тесте
     * @param inputAnswers - список ответов в теста
     * @param inputCorrectAnswers - список номеров правильных ответов
     */
    void createTest(String inputTestName, String inputAuthor, String[] inputQuestions, String[][] inputAnswers, int[] inputCorrectAnswers);
    void deleteTest(int index);
    void runTest (int index);



    /**
     *метод используется для получения имен всех тестов в системе
     * @return - возвращает список имен тестов
     */

    List<String> getTestNames();

    boolean hasNextQuestion();
    String getQuestion();
    String[] getAnswers();
    void sendUserAnswer(int userAnswer);
    int getResult();
}



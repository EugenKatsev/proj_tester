package ru.ewgenij.katsev.tester.model;

import java.io.Serializable;
import java.util.Arrays;

//это само описание класса, из которого позже будет создан конкретный обьект
public class Test implements Serializable {  //implements  Serializable Значит что этот поток может быть куда то передан (по сети или в файловую систему
    private String testName;
    private String author;
    private String[] questions; //Одномерный массив - 0 индекс вопроса
    private String[][] answers; //Двумерный массив 0 индекс вопроса, 0 индекс ответа
    private int[] correctAnswers; //Одномерный массив - для правильный ответов

    //тут создается сам конкретный обьект с конкретными значениями. Это конструктор
    public Test(String inputTestName, String inputAuthor, String[] inputQuestions, String[][] inputAnswers, int[] inputCorrectAnswers){
          //Создаем обьект Тест
          this.testName = inputTestName;
          this.author = inputAuthor;
          this.questions = inputQuestions;
          this.answers = inputAnswers;
          this.correctAnswers = inputCorrectAnswers;

    }


    // возвращаем название теста
    public String getTestName() {
        return testName;

    }
    //возвразаем список вопросов
    public String[] getQuestions() {
        return questions;
    }
    //возвращаем список ответов
    public String[][] getAnswers() {
        return answers;
    }

    //возвращаем список правильных ответов
    public int[] getCorrectAnswers() {
        return correctAnswers;
    }
}
